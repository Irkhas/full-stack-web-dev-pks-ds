<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
    protected $fillable = ['role_id' , 'name' , 'email' , 'username' , 'id'];

    protected $primarykey= 'id';

    protected $keyType = 'string';

    public $incrementing= false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model){
            if (empty ($model->id)) {
                $model->id=Str::uuid();
            }
        });
    }
}
